package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;

public class DBUtils {

    protected static Connection getConnection() throws DBException {
        Connection con = null;
        Properties props = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
            props.load(in);
            String url = props.getProperty("connection.url");
            con = DriverManager.getConnection(url);
        } catch (IOException exception) {
            System.err.println("Cannot find file.");
        } catch (SQLException exception) {
            throw new DBException("err", exception);
        }
        return con;
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                if (!connection.isClosed()) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
