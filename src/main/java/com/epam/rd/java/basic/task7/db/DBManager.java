package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DBManager {
    private static final String SQL_FIND_ALL_USERS = "SELECT id, login FROM users";

    private static final String SQL_INSERT_USER = "INSERT INTO users (login) VALUES (?)";

    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login =?";

    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";

    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";

    private static final String SQL_FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";

    private static final String SQL_INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";

    private static final String SQL_SET_TEAM_FOR_USER =
            "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";

    public static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";

    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

    private static final String SQL_FIND_USER_TEAMS =
            "SELECT teams.id, teams.name\n" +
                    "FROM\n" +
                    "users_teams\n" +
                    "INNER JOIN users  ON users_teams.user_id = users.id\n" +
                    "INNER JOIN teams ON users_teams.team_id = teams.id\n" +
                    "WHERE users.login =?";


    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }


    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DBUtils.getConnection(); Statement stat = con.createStatement()) {
            try (ResultSet rs = stat.executeQuery(SQL_FIND_ALL_USERS)) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String login = rs.getString("login");
                    User user = User.createUser(login);
                    user.setId(id);
                    users.add(user);
                }
            }
        } catch (SQLException exception) {
            System.err.println("Cannot find all users");
            throw new DBException("Cannot find all users", exception);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            String login = user.getLogin();
            stat.setString(1, login);
            if (stat.executeUpdate() > 0) {
                try (ResultSet rs = stat.getGeneratedKeys()) {
                    if (rs.next()) {
                        int id = rs.getInt(1);
                        user.setId(id);
                    }
                }
                return true;
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot insert a new user", exception);
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_DELETE_USER)) {
            for (User user : users) {
                String login = user.getLogin();
                stat.setString(1, login);
                stat.addBatch();
            }
            if (Arrays.stream(stat.executeBatch()).anyMatch(i -> i > 0)) return true;
        } catch (SQLException exception) {
            throw new DBException("Can't delete users", exception);
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_FIND_USER_BY_LOGIN)) {
            stat.setString(1, login);
            try (ResultSet rs = stat.executeQuery()) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    user = User.createUser(login);
                    user.setId(id);
                }
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot find this user", exception);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_FIND_TEAM_BY_NAME)) {
            stat.setString(1, name);
            try (ResultSet rs = stat.executeQuery()) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    team = Team.createTeam(name);
                    team.setId(id);
                }
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot find this team", exception);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DBUtils.getConnection(); Statement stat = con.createStatement()) {
            try (ResultSet rs = stat.executeQuery(SQL_FIND_ALL_TEAMS)) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    Team team = Team.createTeam(name);
                    team.setId(id);
                    teams.add(team);
                }
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot find all teams", exception);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            stat.setString(1, team.getName());
            if (stat.executeUpdate() > 0) {
                try (ResultSet rs = stat.getGeneratedKeys()) {
                    if (rs.next()) {
                        int id = rs.getInt(1);
                        team.setId(id);
                    }
                }
                return true;
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot insert a new team", exception);
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        boolean result = false;
        Connection con = null;
        try {
            con = DBUtils.getConnection();
            con.setAutoCommit(false);
            try (PreparedStatement stat = con.prepareStatement(SQL_SET_TEAM_FOR_USER)) {
                for (Team team : teams) {
                    stat.setInt(1, user.getId());
                    stat.setInt(2, team.getId());
                    stat.addBatch();
                }
                if (Arrays.stream(stat.executeBatch()).anyMatch(i -> i > 0)) result = true;
            }
            con.commit();
        } catch (SQLException exception) {
            try {
                con.rollback();
                throw new DBException("err", exception);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            DBUtils.close(con);
        }
        return result;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_FIND_USER_TEAMS)) {
            stat.setString(1, user.getLogin());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    Team team = Team.createTeam(name);
                    team.setId(id);
                    teams.add(team);
                }
            }
        } catch (SQLException exception) {
            throw new DBException("Cannot get all teams for the user", exception);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_DELETE_TEAM)) {
            stat.setString(1, team.getName());
            if (stat.executeUpdate() > 0) return true;
        } catch (SQLException exception) {
            throw new DBException("Can't delete user:" + exception.getMessage(), exception);
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DBUtils.getConnection();
             PreparedStatement stat = con.prepareStatement(SQL_UPDATE_TEAM)) {
            stat.setString(1, team.getName());
            stat.setInt(2, team.getId());
            if (stat.executeUpdate() > 0) return true;
        } catch (SQLException exception) {
            throw new DBException("Can't update team:" + exception.getMessage(), exception);
        }
        return false;
    }
}
